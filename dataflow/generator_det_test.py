from finding_common_object.dataflow.generator_det import GeneratorDet
import tensorflow as tf
from finding_common_object.dataflow.utils import fix_rng_seed
k_shot=2
output_im_shape=(512,512)
fea_dim=4096
bag_size=100
meta_batch_size=4
fix_rng_seed(1357)

generator = GeneratorDet(root='/nv/hmart1/ashaban6/scratch/Dataset', split='alexfea_source_ex', k_shot=k_shot, pos_threshold=0.7, output_im_shape=output_im_shape, objectness_key='objectness_1-bg',feature_normalization='l2', bag_size=bag_size)
generator.reset_state()
x = next(iter(generator.get_data()))
from IPython import embed;embed()
dataset = tf.data.Dataset.from_generator(generator.get_data, (tf.float32, tf.float32, tf.float32,
                                                              tf.float32, tf.float32, tf.float32,
                                                              tf.int32, tf.int32,
                                                              tf.string, (tf.float32, tf.float32),
                                                              (tf.float32, tf.float32)))
#print("dataset");from IPython import embed;embed()
ds2 = dataset.padded_batch(meta_batch_size,
                               ((k_shot,bag_size,1,1,fea_dim), (k_shot,bag_size,4),
                               (k_shot,bag_size), (k_shot,bag_size),
                               (k_shot,)+output_im_shape+(3,), (k_shot,bag_size), (),
                               (k_shot,), (k_shot,), ((None,4),)*k_shot, ((None,),)*k_shot))
a = next(iter(ds2))
(feas, fea_boxes, fea_target_classes,
 fea_classes, imgs, objectness, target_class,
 num_objects, img_fns, boxes_tuple, classes_tuple) = a
from IPython import embed;embed()

