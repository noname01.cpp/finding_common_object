from finding_common_object.dataflow.data_sampler_det import DataSamplerDet
import os.path as osp
import numpy as np
import cv2
from finding_common_object.dataflow.base import RNGDataFlow

class GeneratorDet(RNGDataFlow):
  def __init__(self,
               root,
               split,
               k_shot=1,
               noise_rate=0.0,
               class_agnostic=False,
               num_negative_bags=0,
               min_nclasses_in_positive_images=0,
               smart_neg_bag_sampler=False,
               pos_threshold=0.5,
               output_im_shape=None,
               objectness_key='objectness',
               mode=None,
               folds=None,
               feature_normalization=None,
               bag_size=None,
               feas_key='feas'):

    self.root = root
    self.k_shot = k_shot
    self.split = split
    self.class_agnostic = class_agnostic
    self.noise_rate = noise_rate
    self.num_negative_bags = num_negative_bags
    self.min_nclasses_in_positive_images = min_nclasses_in_positive_images
    self.pos_threshold = pos_threshold
    self.output_im_shape = tuple(output_im_shape)
    self.objectness_key = objectness_key
    self.mode = mode
    self.folds = folds
    self.smart_neg_bag_sampler = smart_neg_bag_sampler
    self.sampler = None #init_data_sampler()

    assert(feature_normalization in [None, '', 'l2'])
    self.feature_normalization = feature_normalization
    self.bag_size = bag_size
    self.feas_key = feas_key

  def init_data_sampler(self):
     self.sampler = DataSamplerDet(self.root, self.split,
                                  self.rng, self.k_shot,
                                  self.num_negative_bags,
                                  self.noise_rate,
                                  self.class_agnostic,
                                  min_nclasses_in_positive_images=self.min_nclasses_in_positive_images,
                                  smart_neg_bag_sampler=self.smart_neg_bag_sampler,
                                  pos_threshold=self.pos_threshold,
                                  objectness_key=self.objectness_key,
                                  mode=self.mode,
                                  folds=self.folds,
                                  feas_key=self.feas_key)

  def size(self):
    return self.sampler.cls_reader.nr_images

  def _load_image_and_feas(self, data_fns):
    imgs, feas, img_fns = [], [], []
    for fn in data_fns:
      img_path = osp.join(self.root, 'Images', fn[0])
      img_fns.append(fn[0])
      fea_path = osp.join(self.root, 'Feas', fn[1])
      img = cv2.imread(img_path, cv2.IMREAD_COLOR)
      if self.output_im_shape is not None and img.shape[:2] != self.output_im_shape:
        img = cv2.resize(img, self.output_im_shape[::-1])
      fea = np.load(fea_path)
      if isinstance(fea, np.lib.npyio.NpzFile):
        assert 'fea' in fea, 'fea is not in npz file'
        fea = fea['fea']
      imgs.append(img)
      feas.append(fea)
    return imgs, feas, img_fns

  def _rel_to_abs(self, b, image_shape):
      assert(isinstance(b, np.ndarray))
      assert(np.all(np.logical_and(0 <= b, b <= 1)))
      b = np.array(b)
      b[...,0::2] *= image_shape[0]
      b[...,1::2] *= image_shape[1]
      return b

  def format_boxes(self, boxes, image_shape):
    xy_format, coordinates_format = self.sampler.meta.split(',')
    assert(xy_format == 'y0x0y1x1')
    assert(coordinates_format  == 'relative'), 'Only supports relative bb format.'

    #if coordinates_format == 'relative':
    #  if isinstance(boxes, list):
    #    boxes = [self._rel_to_abs(b, image_shape) for b in boxes]
    #  else:
    #    boxes = self._rel_to_abs(boxes, image_shape)
    return boxes

  def get_meta_data(self):
    if self.sampler is None:
      self.init_data_sampler()

    for _ in list(range(self.size())):
      (data_fns, boxes, classes, fea_boxes,
          fea_classes, objectness, target_class) = self.sampler.next()

      num_objs = np.array([len(b) for b in boxes])

      yield (tuple(data_fns), tuple(boxes), tuple(classes),
             tuple(fea_boxes), tuple(fea_classes), tuple(objectness),
             target_class, num_objs)

  def consume_meta_data(self, inputs):
    (data_fns, boxes, classes,
            fea_boxes, fea_classes, objectness, target_class, num_objs) = inputs
    imgs, feas, img_fns = self._load_image_and_feas(data_fns)
    boxes = self.format_boxes(boxes, imgs[0].shape[:2])
    fea_boxes = self.format_boxes(np.stack(fea_boxes), imgs[0].shape[:2])
    fea_target_classes = []
    for fea_class in fea_classes:
      fea_target_class = np.zeros_like(fea_class)
      fea_target_class[fea_class == target_class] = 1
      fea_target_classes.append(fea_target_class)

    feas = np.stack(feas)
    if self.feature_normalization == 'l2':
      feas = feas / (1e-12 + np.linalg.norm(feas, axis=-1)[..., np.newaxis])
    num_objs = np.array([len(b) for b in boxes])
    ret = (feas, fea_boxes, np.stack(fea_target_classes),
           np.stack(fea_classes), np.stack(imgs), np.stack(objectness),
           target_class, num_objs,
           img_fns, boxes, classes)
    if self.bag_size is not None:
      def get_first_bag_size_elements(arr_tuple, indices):
        arr_list = list(arr_tuple)
        for ind in indices:
          arr_list[ind] = arr_list[ind][:,:self.bag_size]
        return tuple(arr_list)
      ret = get_first_bag_size_elements(ret, [0,1,2,3,5])
    return ret


  def get_data(self):
    """
    Returns:
      feas: [total_bags, bag_size, 1,1,fea_dim] np.ndarray of feature array
      fea_boxes: [total_bags, bag_size, 4] np.ndarrayt of feature boxes
      fea_target_classes: [total_bags, bag_size]  0/1 np.ndarray of target class indicators
      fea_classes: [total_bags, bag_size] np.ndarray of classes for each box
      imgs: [total_bags, input_im_h, input_im_w, 3] uint8 np.ndarray of images
      objectness: [total_bags, bag_size] np.ndarray of objectness score for each box
      target_class: integer of target class
      num_objs: (total_bags,) np.ndarray showing number of objects in each bag
      img_fns: (total_bags,) string list of image file names
      boxes: (total_bags,) tuple of [n,4] float32 groundtruth boxes for each bag
      classes: (total_bags,) tuple of [n,] float32 groundtruth classes for each bag
    """
    if self.sampler is None:
      self.init_data_sampler()

    for _ in list(range(self.size())):
      (data_fns, boxes, classes, fea_boxes,
          fea_classes, objectness, target_class) = self.sampler.next()
      imgs, feas, img_fns = self._load_image_and_feas(data_fns)
      boxes = self.format_boxes(boxes, imgs[0].shape[:2])
      fea_boxes = self.format_boxes(np.stack(fea_boxes), imgs[0].shape[:2])
      fea_target_classes = []
      for fea_class in fea_classes:
        fea_target_class = np.zeros_like(fea_class)
        fea_target_class[fea_class == target_class] = 1
        fea_target_classes.append(fea_target_class)

      feas = np.stack(feas)
      if self.feature_normalization == 'l2':
        feas = feas / (1e-12 + np.linalg.norm(feas, axis=-1)[..., np.newaxis])
      num_objs = np.array([len(b) for b in boxes])
      ret = (feas, fea_boxes, np.stack(fea_target_classes),
             np.stack(fea_classes), np.stack(imgs), np.stack(objectness),
             target_class, num_objs,
             img_fns, tuple(boxes), tuple(classes))
      if self.bag_size is not None:
        def get_first_bag_size_elements(arr_tuple, indices):
          arr_list = list(arr_tuple)
          for ind in indices:
            arr_list[ind] = arr_list[ind][:,:self.bag_size]
          return tuple(arr_list)
        ret = get_first_bag_size_elements(ret, [0,1,2,3,5])
      yield ret

