import tensorflow as tf
import utils
from model.inference_model import InferenceModel
import os.path as osp
import logging
import argparse
from IPython import embed
import time
import numpy as np
from collections import defaultdict
import pickle

parser = argparse.ArgumentParser()
parser.add_argument('--experiments_dir', required=True,
          help="Directory containing the experiments")
parser.add_argument('--old_weights', required=True,
          help="Path to old weights")

def save(config, dataset, old_weights):
  logging.info('Running evaluation for {} iterations'.format(config.iters))
  model = InferenceModel(config)
  checkpoint = config.checkpoint
  if osp.isdir(config.checkpoint):
    checkpoint = tf.train.latest_checkpoint(config.checkpoint)

  logging.info('Restoring parameters from {}'.format(checkpoint))
  ckpt = tf.train.Checkpoint(net=model)
  ckpt.restore(checkpoint).expect_partial()
  corloc_list = []
  multiclass_corloc_dict = defaultdict(list)
  inference_time_list = []
  dt = next(iter(dataset))
  inputs = utils.parse_dt(dt, config, training=False)
  top_subproblem, labels, target_class = model(inputs, training=False)
  with open(old_weights, 'rb') as f:
    w = pickle.load(f)
  d = {}
  var_names = ['score_fc', 'tanh_fc', 'tanh_bn', 'sigm_fc', 'sigm_bn']
  for name in var_names:
    d['transfered_'+name] = [(v.numpy(), v.name) for v in getattr(model._relation_module,
                                                                  name).variables]
  pre_var_names = ['pre_fc', 'pre_bn']
  for name in pre_var_names:
    d['transfered_'+name] = [(v.numpy(), v.name) for v in getattr(model,name).variables]
  for k,v in d.items():
    w_v = w[k]
    for i,(var,name) in enumerate(v):
      assert(name == w_v[i][1])
      assert(var.shape == w_v[i][0].shape)
  w.update(d)
  with open('weights.pkl', 'wb') as f:
    pickle.dump(w, f)


def main():
  #tf.config.gpu.set_per_process_memory_growth(True)
  # Load the config from json file
  args = parser.parse_args()
  json_path = osp.join(args.experiments_dir, 'config.json')
  assert osp.isfile(json_path), "No json configuration file found at {}".format(json_path)
  config = utils.get_config(json_path)
  utils.set_logger(osp.join(args.experiments_dir, 'test.log'))
  logging.info('Loading the dataset...')
  test_dataset = utils.get_dataset(config.test, training=False)
  logging.info('- done.')
  save(config.test, test_dataset, args.old_weights)

if __name__ == '__main__':
  main()

