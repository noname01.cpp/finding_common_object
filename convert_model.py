import tensorflow as tf
import utils
from model.inference_model import InferenceModel
import os.path as osp
import logging
import argparse
from IPython import embed
import numpy as np
from tensorflow.python.pywrap_tensorflow import NewCheckpointReader

def find(names_and_vars, keys):
  if not isinstance(keys, list):
    keys = [keys]
  ret = []
  for name,var in names_and_vars:
    if all([key in name for key in keys]):
      ret.append((name,var))
  return ret

def copy_var(from_names_and_vars, to_var, keys):
    name, from_var = find(from_names_and_vars, keys)[0]
    to_shape = to_var.shape.as_list()
    print('{} {} ==> {} {}'.format(name, from_var.shape, to_var.name, to_shape))
    from_var = from_var.reshape(to_shape)
    to_var.assign(from_var)

def copy_bn(from_names_and_vars, to_vars):
    names = ['gamma', 'beta', 'moving_mean', 'moving_variance']
    assert(len(from_names_and_vars) == len(to_vars))
    assert(len(to_vars) == len(names))
    for name, to_var  in zip(names, to_vars):
      assert(name in to_var.name)
      copy_var(from_names_and_vars, to_var, name)

def copy_relation_module(from_names_and_vars, to_vars, cur_unit):
  # Copy tanh bn
  fnv = find(from_names_and_vars, [cur_unit, 'tanh', 'BatchNorm'])
  copy_bn(fnv, to_vars[4:6] + to_vars[8:10])

  # Copy sigmoid bn
  fnv = find(from_names_and_vars, [cur_unit, 'sigmoid', 'BatchNorm'])
  copy_bn(fnv, to_vars[6:8] + to_vars[10:12])

  # Copy tanh weights
  copy_var(from_names_and_vars, to_vars[0], [cur_unit, 'tanh', 'weights'])

  # Copy sigmoid weights
  copy_var(from_names_and_vars, to_vars[1], [cur_unit, 'sigmoid', 'weights'])

  # Copy score weights
  copy_var(from_names_and_vars, to_vars[2], [cur_unit, 'score', 'weights'])
  copy_var(from_names_and_vars, to_vars[3], [cur_unit, 'score', 'bias'])

def read_source(source_fn):
  reader = NewCheckpointReader(source_fn)
  shape_map = reader.get_variable_to_shape_map()
  names = list(shape_map.keys())

  names_and_vars = []
  for name in names:
    if 'Momentum' not in name:
      names_and_vars.append((name, reader.get_tensor(name)))
  return names_and_vars

def convert(config, dataset):
  model = InferenceModel(config)
  dt = next(iter(dataset))
  inputs = utils.parse_dt(dt, config, training=False)
  model(inputs, training=False)

  names_and_vars = read_source(config.source)

  [v.assign(tf.zeros_like(v)) for v in model.variables]

  # copy pre_fc weights
  if 'pre_fc' in config:
    #copy fc
    copy_var(names_and_vars, model.pre_fc.variables[0], ['Preprocess', 'weights'])
    #copy bn
    from_names_and_vars = find(names_and_vars, ['Preprocess', 'BatchNorm'])
    copy_bn(from_names_and_vars, model.pre_bn.variables)

  # copy unary weights
  if config.negative_bag_size > 0:
    cur_unit = 'Unit0'
    u_vars = [v for v in model._unary_module._relation_unit.variables]
    from_names_and_vars = [name_and_var for name_and_var in names_and_vars if cur_unit in name_and_var[0]]
    copy_relation_module(from_names_and_vars, u_vars, cur_unit)
    to_var = [v for v in model._unary_module.variables if 'softmax' in v.name][0]
    copy_var(names_and_vars, to_var, ['negative_softmax_param'])

  # copy pairwise weights
  cur_unit = 'Unit1'
  p_vars = [v for v in model._relation_module.variables]
  from_names_and_vars = [name_and_var for name_and_var in names_and_vars if cur_unit in name_and_var[0]]
  copy_relation_module(from_names_and_vars, p_vars, cur_unit)

  ckpt = tf.train.Checkpoint(step=tf.Variable(0), net=model)
  manager = tf.train.CheckpointManager(ckpt, config.checkpoint,
                                       max_to_keep=1)
  save_path = manager.save()
  logging.info('Saved')

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--experiments_dir', required=True,
          help="Directory containing the experiments")
  parser.add_argument('--source', required=True,
          help="Path to the source model ckpt")
  #tf.config.gpu.set_per_process_memory_growth(True)
  # Load the config from json file
  args = parser.parse_args()
  json_path = osp.join(args.experiments_dir, 'config.json')
  assert osp.isfile(json_path), "No json configuration file found at {}".format(json_path)
  config = utils.get_config(json_path)
  utils.set_logger(osp.join(args.experiments_dir, 'test.log'))
  logging.info('Loading the dataset...')
  test_dataset = utils.get_dataset(config.test, training=False)
  logging.info('- done.')
  config.test.source = args.source
  convert(config.test, test_dataset)

if __name__ == '__main__':
  main()

