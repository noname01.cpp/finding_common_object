import tensorflow as tf
import utils
from model.inference_model import InferenceModel
import os.path as osp
import logging
import argparse
from IPython import embed
import time
import numpy as np
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument('--experiments_dir', required=True,
          help="Directory containing the experiments")

def eval(config, dataset):
  logging.info('Running evaluation for {} iterations'.format(config.iters))
  model = InferenceModel(config)
  checkpoint = config.checkpoint
  if osp.isdir(config.checkpoint):
    checkpoint = tf.train.latest_checkpoint(config.checkpoint)

  logging.info('Restoring parameters from {}'.format(checkpoint))
  ckpt = tf.train.Checkpoint(net=model)
  ckpt.restore(checkpoint).expect_partial()
  corloc_list = []
  multiclass_corloc_dict = defaultdict(list)
  inference_time_list = []
  for step, dt in enumerate(dataset):
    inputs = utils.parse_dt(dt, config, training=False)
    start_time = time.time()
    top_subproblem, labels, target_class = model(inputs, training=False)
    inference_time_list.append(time.time() - start_time)
    utils.corloc(top_subproblem, labels, corloc_list)
    utils.multiclass_corloc(top_subproblem, labels, multiclass_corloc_dict, target_class)
    if (step+1) % config.print_freq == 0:
      logging.info('step {}/{} ({:0.3f} sec/iter)'.format(step+1, config.iters,
                            np.mean(inference_time_list[-config.print_freq:])))
    if step >= config.iters:
      break

  mean_corloc = tf.reduce_mean(corloc_list).numpy()
  std_corloc = tf.math.reduce_std(corloc_list).numpy()
  classes_corloc = [tf.reduce_mean(v) for k,v in multiclass_corloc_dict.items()]
  mc_mean_corloc = tf.reduce_mean(classes_corloc).numpy()
  mc_std_corloc = tf.math.reduce_std(classes_corloc).numpy()
  print('- done.')
  logging.info('Accuracy is {} +- {}'.format(mean_corloc, std_corloc))
  logging.info('Multiclass accuracy is {} +- {}'.format(mc_mean_corloc, mc_std_corloc))
  return mean_corloc, std_corloc, corloc_list

def main():
  #tf.config.gpu.set_per_process_memory_growth(True)
  # Load the config from json file
  args = parser.parse_args()
  json_path = osp.join(args.experiments_dir, 'config.json')
  assert osp.isfile(json_path), "No json configuration file found at {}".format(json_path)
  config = utils.get_config(json_path)
  utils.set_logger(osp.join(args.experiments_dir, 'test.log'))
  logging.info('Loading the dataset...')
  test_dataset = utils.get_dataset(config.test, training=False)
  logging.info('- done.')
  eval(config.test, test_dataset)

if __name__ == '__main__':
  main()

