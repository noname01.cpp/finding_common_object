import tensorflow as tf
from finding_common_object.dataflow.generator_cls import GeneratorCLS
from finding_common_object.dataflow.generator_det import GeneratorDet
import json
import logging
from easydict import EasyDict
from finding_common_object.dataflow.utils import fix_rng_seed
from finding_common_object.model.util import batched_gather
import os
import pickle

def get_pairwise_loss(config, labels, logits):
  loss_type = 'sigmoid'
  if 'loss' in config:
    loss_type = config.loss.type
  if loss_type == 'sigmoid':
    loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels,
                                                   logits=logits)
    return tf.reduce_mean(loss)
  elif loss_type == 'focal':
    loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels,
                                                   logits=logits)
    preds = tf.nn.sigmoid(logits)
    weights = tf.where(tf.cast(labels, dtype=tf.bool), 1 - preds, preds)
    weights = config.loss.alpha * tf.pow(weights, config.loss.gamma)
    loss = weights * loss
    return tf.reduce_mean(loss)
  else:
    raise ValueError("Loss type {} is not defined".format(loss_type))

def get_lr_schedule(lr_schedule):
  boundaries = lr_schedule.boundaries
  values = lr_schedule.values
  return tf.keras.optimizers.schedules.PiecewiseConstantDecay(boundaries, values)

def corloc(top_subproblem, labels, corloc_list):
  ntotal = tf.cast(tf.shape(top_subproblem)[-1], tf.float32)
  for i in range(tf.shape(labels)[0]):
    res = batched_gather(top_subproblem[i, ..., tf.newaxis],
                         labels[i])
    corloc_list.append(tf.reduce_sum(res)/ntotal)

def multiclass_corloc(top_subproblem, labels, corloc_dict, target_class):
  ntotal = tf.cast(tf.shape(top_subproblem)[-1], tf.float32)
  for i in range(tf.shape(labels)[0]):
    res = batched_gather(top_subproblem[i, ..., tf.newaxis],
                         labels[i])
    corloc_dict[target_class[i].numpy()].append(tf.reduce_sum(res)/ntotal)

def get_best_acc(dir_path):
  top_pkl_file = os.path.join(dir_path, 'top.pkl')
  if os.path.isfile(top_pkl_file):
    with open(top_pkl_file, 'rb') as f:
      top = pickle.load(f)
    return top['best']
  return 0.0

def save_best_acc(dir_path, best_acc, iteration):
  top_pkl_file = os.path.join(dir_path, 'top.pkl')
  top = {'best': best_acc, 'iteration': iteration}
  with open(top_pkl_file, 'wb') as f:
    pickle.dump(top, f)

def get_dataset_cls(config, training):
  num_negative_bags = config.negative_bag_size // config.bag_size
  gen = GeneratorCLS(is_training=config.is_training, shuffle=config.shuffle,
                     add_gt_list=False, k_shot=config.k_shot, #TODO now cannot return the list now
                     bag_size=config.bag_size, num_negative_bags=num_negative_bags,
                     split=config.split, num_sample_classes=config.num_sample_classes,
                     num_sample_classes_min=config.num_sample_classes_min, use_features=config.use_features,
                     dataset_name=config.dataset_name, one_example_per_class=config.one_example_per_class,
                     has_single_target=config.has_single_target)
  gen.reset_state()
  dataset = tf.data.Dataset.from_generator(gen.get_data,
                                            (tf.float32, tf.float32, tf.float32,
                                             tf.float32, tf.float32, tf.int32)).prefetch(
                                                 config.prefetch_buffer_size)
  if training:
    return dataset.batch(config.meta_batch_size).repeat()
  else:
    return dataset.batch(1) #NOTE: we can repeat since new problems will be different
  return dataset

def get_dataset_det(config, training):
  k_shot = config.k_shot
  bag_size = config.bag_size
  fea_dim = config.fea_dim
  output_im_shape = tuple(config.output_im_shape)
  num_negative_bags = config.negative_bag_size // bag_size

  gen = GeneratorDet(root=config.root,
                     split=config.split,
                     k_shot=config.k_shot,
                     noise_rate=config.noise_rate,
                     class_agnostic=config.class_agnostic,
                     num_negative_bags=num_negative_bags,
                     min_nclasses_in_positive_images=config.min_nclasses_in_positive_images,
                     smart_neg_bag_sampler=config.smart_neg_bag_sampler,
                     pos_threshold=config.pos_threshold,
                     output_im_shape=output_im_shape,
                     objectness_key=config.objectness_key,
                     mode=config.mode,
                     folds=config.folds,
                     feature_normalization=config.feature_normalization,
                     bag_size=config.bag_size,
                     feas_key=config.feas_key)
  gen.reset_state()
  dataset = tf.data.Dataset.from_generator(gen.get_data, (tf.float32, tf.float32, tf.float32,
                                                          tf.float32, tf.float32, tf.float32,
                                                          tf.int32, tf.int32,
                                                          tf.string, ((tf.float32,))*k_shot,
                                                          ((tf.float32,))*k_shot)).prefetch(
                                                              config.prefetch_buffer_size)
  if training:
    dataset = dataset.padded_batch(config.meta_batch_size,
                                   ((k_shot,bag_size,1,1,fea_dim), (k_shot,bag_size,4),
                                   (k_shot,bag_size), (k_shot,bag_size),
                                   (k_shot,)+output_im_shape+(3,), (k_shot,bag_size), (),
                                   (k_shot,), (k_shot,), ((None,4),)*k_shot, ((None,),)*k_shot)).repeat()
  else:
    dataset = dataset.batch(1)
  return dataset

def get_dataset(config, training):
  if config.shuffle is False:
    fix_rng_seed(config.seed)
  if not 'problem_type' in config:
    config.problem_type = 'classification'

  if config.problem_type == 'classification':
    dataset = get_dataset_cls(config, training)
  elif config.problem_type == 'detection':
    dataset = get_dataset_det(config, training)
  else:
    raise ValueError("Invalid problem_type: {}".format(problem_type))
  return dataset

def parse_dt_cls(dt, config):
    fea, _, _, classes, _, target_class = dt
    pos_fea = fea[:, :config.k_shot, :, 0, 0]
    neg_fea = fea[:, config.k_shot:, :, 0, 0]
    neg_shape = tf.shape(neg_fea)
    ## [MBS, N_NEG_BAGS, BAG_SIZE, D] ==> [MBS, N_NEG_BAGS*BAG_SIZE, D]
    neg_fea = tf.reshape(neg_fea, [neg_shape[0], -1, neg_shape[-1]])

    pos_classes = classes[:, :config.k_shot]
    neg_classes = classes[:, config.k_shot:]
    ## [MBS, N_NEG_BAGS, BAG_SIZE] ==> [MBS, N_NEG_BAGS*BAG_SIZE]
    neg_classes = tf.reshape(neg_classes, [neg_shape[0], -1])
    return [pos_fea, neg_fea, pos_classes, neg_classes, target_class]

def parse_dt_det(dt, config, training):
  (feas, fea_boxes, fea_target_classes,
   fea_classes, imgs, objectness, target_class,
   num_objects, img_fns, boxes_tuple, classes_tuple) = dt
  pos_fea = feas[:, :config.k_shot, :, 0,0]
  neg_fea = feas[:, config.k_shot:, :, 0,0]
  neg_shape = tf.shape(neg_fea)
  neg_fea = tf.reshape(neg_fea, [neg_shape[0], -1, neg_shape[-1]])
  pos_classes = fea_classes[:, :config.k_shot]
  neg_classes = fea_classes[:, config.k_shot:]
  ## subsample
  if training  and 'subsample' in config:
    if config.subsample.agnostic:
      is_target = tf.ones_like(pos_classes) * tf.cast(config.bg_class, tf.float32)
      is_target = tf.not_equal(is_target, pos_classes)
    else:
      is_target = tf.ones_like(pos_classes) * tf.cast(target_class, tf.float32)[:,None,None]
      is_target = tf.equal(is_target, pos_classes)
    def fn(itarget):
      p_indices = tf.where(itarget)[...,0]
      n_indices = tf.where(tf.logical_not(itarget))[..., 0]
      p_indices = tf.random.shuffle(p_indices)
      n_indices = tf.random.shuffle(n_indices)
      rem = config.subsample.nsamples
      if tf.shape(n_indices)[0] > 0:
        npos = tf.minimum(config.subsample.npos, tf.shape(p_indices)[0])
      else:
        npos = config.subsample.nsamples
      rem = rem - npos
      inds = p_indices[:npos]
      if tf.shape(n_indices)[0] < rem and rem > 0:
        n_indices = tf.tile(n_indices, [config.subsample.nsamples])
      inds = tf.concat([inds, n_indices[:rem]], axis=0)
      return tf.random.shuffle(inds)

    pclasses_list = []
    pfea_list = []
    for pfea, pclasses, itarget in zip(pos_fea, pos_classes,is_target):
      left_inds = fn(itarget[0])
      right_inds = fn(itarget[1])
      pfea_left = tf.gather(pfea[0], left_inds)
      pclasses_left = tf.gather(pclasses[0], left_inds)
      pfea_right = tf.gather(pfea[1], right_inds)
      pclasses_right = tf.gather(pclasses[1], right_inds)
      pclasses_list.append(tf.stack([pclasses_left, pclasses_right]))
      pfea_list.append(tf.stack([pfea_left, pfea_right]))
    pos_classes = tf.stack(pclasses_list)
    pos_fea = tf.stack(pfea_list)
  ##
  ## [MBS, N_NEG_BAGS, BAG_SIZE] ==> [MBS, N_NEG_BAGS*BAG_SIZE]
  neg_classes = tf.reshape(neg_classes, [neg_shape[0], -1])
  return [pos_fea, neg_fea, pos_classes, neg_classes, target_class]

def parse_dt(dt, config, training):
  if config.problem_type == 'classification':
    return parse_dt_cls(dt,config)
  elif config.problem_type == 'detection':
    return parse_dt_det(dt,config,training)

def get_config(path):
  with open(path,'r') as f:
    return EasyDict(json.load(f))

def set_logger(log_path):
  """Set the logger to log info in terminal and file `log_path`.

  In general, it is useful to have a logger so that every output to the terminal is saved
  in a permanent file. Here we save it to `model_dir/train.log`.

  Example:
  ```
  logging.info("Starting training...")
  ```

  Args:
      log_path: (string) where to log
  """
  logger = logging.getLogger()
  logger.setLevel(logging.INFO)

  if not logger.handlers:
    # Logging to a file
    file_handler = logging.FileHandler(log_path)
    file_handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
    logger.addHandler(file_handler)

    # Logging to console
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter('%(message)s'))
    logger.addHandler(stream_handler)
